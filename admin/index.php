<?php

    require('../includes/config.php');
    require_once('../classes/login.class.php');
    require_once('../classes/polladmin.class.php');
    // require_once '../includes/Twig/Autoloader.php';
    // Twig_Autoloader::register();

    session_start();
       
    $loader = new Twig_Loader_Filesystem('template/');
    $twig = new Twig_Environment($loader, array(
        //'cache' => '../cache',
        
    ));
    
    $loginc = new login($db);
    $poll_admin = new Polladmin($db, $twig);
    
    $do = $_GET['do'];
    $cases_template_active = array('main','add','edit');
    if(in_array($do, $cases_template_active)){
        echo $twig->render('header.html',array('user' => $_SESSION['user']));
    }

    switch($do){

        case'main':
            $loginc->checkLoginEachPage();
            $poll_admin->show();
        break;

        case'check':
            $loginc->check($_POST['username'],$_POST['password']);
        break;

        case'logout':
            $loginc->Logout('index.php');
        break;

        default:
            //$loginc->checkLoginEachPage();
            echo $twig->render('login.html', array('user' => 'שם משתמש','pass' => 'סיסמה','logint' => 'כניסה'));
        break;

        case'add':
            $loginc->checkLoginEachPage();
            $twig->display('add_poll.html');
        break;

        case'add_exc':
            $poll_admin->add_exc($_POST['question'], $_POST['answer']);
        break;
    
        case'edit':
            $loginc->checkLoginEachPage();
            $qid = $_GET['id'];
            $poll_admin->edit($qid);
        break;
    
        case'edit_exc':
            $poll_admin->edit_exc($_POST['QID'], $_POST['question'], $_POST['answer']);
        break;

        case'delete':
            $qid = $_GET['id'];
            $poll_admin->delete($qid);
            echo general::Ref('index.php?do=main');
        break;

        case'del_answer':
            $id = $_GET['id'];
            $question = $_GET['question'];
            $poll_admin->delete_answer($id, $question);
            echo general::Ref("index.php?do=edit&id=$question");
        break;

        case'asnwers_show':
            $qid = $_GET['qid'];
            $is_pie = $_GET['is_pie'];
            $loginc->checkLoginEachPage();
            $poll_admin->show_answers($qid, $is_pie);
        break;
    }

    if(in_array($do, $cases_template_active)){
        echo $twig->display('footer.html');
    }
