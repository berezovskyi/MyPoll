<?php
    require_once('../vendor/autoload.php');

    // $db_dsn = 'mysql:host=localhost;dbname=mypoll;charset=utf8';
    $db_dsn = 'pgsql:host=localhost;dbname=mypoll';
    $db_user = 'postgres';
    $db_pass = getenv("DB_PASS");
    $db_options = array(
        PDO::ATTR_EMULATE_PREPARES => false,
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
    );

    require_once('../classes/general.class.php');
    require_once('../classes/db.class.php');
