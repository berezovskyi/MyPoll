<?php

    require_once("config.php");

    $aquery = <<<STR
            CREATE table `Questions` (
            `ID` int(11) NOT NULL AUTO_INCREMENT,
            `QUESTION` varchar (300) NOT NULL,
            PRIMARY KEY (`ID`)
            )
            ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;

            CREATE table `Answers` (
            `ID` int(11) NOT NULL AUTO_INCREMENT,
            `QID` int (11) NOT NULL,
            `ANSWER` varchar (300) NOT NULL,
            `VOTES` int (11) NOT NULL,
            PRIMARY KEY (`ID`)
            )
            ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;

            CREATE table `Users` (
            `ID` int(11) NOT NULL AUTO_INCREMENT,
            `UNAME` varchar (300) NOT NULL,
            `UPASS` varchar (32) NOT NULL,
            `EMAIL` varchar (300) NOT NULL,
            PRIMARY KEY (`ID`)
            )
            ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;

            INSERT INTO `Users` VALUES (1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'ebncana@hotmail.com');
STR;

    $db->run($aquery, $bind="", 'Database created Successfully');
