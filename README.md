Poll application
=======================

Every successful git-based project starts with a README.

Purpose of a README file is to describe the project goal, installation steps,
simple use case (like a 1-minute tutorial) and set the guidelines for contributing
the source code to the future authors.

## Run

In order to run the application, run 

    composer install

for the first time. After that,

    cd admin
    export DB_PASS="*******"; php -S localhost:8000

The website is ready on http://localhost:8000

The script for PostgreSQL can be found [here](https://gist.github.com/berezovskyi/4ff41f83af83c2d74383).
