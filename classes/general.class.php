<?php

    class general{

        static function array_avg($arrays){
            $myarr = $arrays;
            $sum = array_sum(array_map(function ($a) { return $a[1]; }, $myarr));
            // walk through the array, print the percentage (value / sum) for each browser
            foreach ($myarr as $info) {
                echo round(($info[1]/$sum)*100);
            }
            //return $result;
        }

        static function Ref($url){
            return '<meta http-equiv="refresh" content="0; url='.$url.'">';
        }

        static function MsgS($msg, $type, $url=""){
            echo $msg;
            if(!empty($url)){
            return '<meta http-equiv="refresh" content="2; url='.$url.'">';
            }
        }

        static function selfURL() {
            $s = empty($_SERVER["HTTPS"]) ? ''
                : ($_SERVER["HTTPS"] == "on") ? "s"
                    : "";
            $protocol = strleft(strtolower($_SERVER["SERVER_PROTOCOL"]), "/").$s;
            $port = ($_SERVER["SERVER_PORT"] == "80") ? ""
                : (":".$_SERVER["SERVER_PORT"]);
            return $protocol."://".$_SERVER['SERVER_NAME'].$port.$_SERVER['REQUEST_URI'];
        }

        static function strleft($s1, $s2) {
            return substr($s1, 0, strpos($s1, $s2));
        }
    }