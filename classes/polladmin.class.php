<?php

    class Polladmin{
        
        public $twig;
        protected $db;

        public function __construct($db, $twig){
            $this->db = $db;
            $this->twig = $twig;
        }

        public function add_exc($question, $answers){
 
            $insert_q = $this->db->prepare("INSERT INTO questions (question) VALUES (:question)");
            $insert_q->bindValue(':question', $question, PDO::PARAM_STR);
            try{
                $insert_q->execute();
                echo 'Question added';
            }catch (Exception $e){
                echo'Error :' . $e->getMessage();
            }

            $lastid_result = $this->db->lastInsertId();

            foreach($answers as $valuex){
                $insert_a = $this->db->prepare('INSERT INTO answers (answer,qid,votes) VALUES (:answer,:qid,0)');
                $insert_a->bindValue(':answer', $valuex, PDO::PARAM_STR);
                $insert_a->bindValue(':qid', $lastid_result, PDO::PARAM_INT);
                $insert_a->execute();
            }
        }
        
        public function show(){
            $resultsp = $this->db->prepare('SELECT * FROM questions');
            $resultsp->execute();
            $results = $resultsp->fetchAll(PDO::FETCH_ASSOC);
            echo $this->twig->render('show_poll.html', array('resultsp' => $results));
        }

        public function show_answers($qid, $is_pie)
        {
            //$questions = $this->db->query('SELECT id FROM questions');
            //$questions_num = count($questions);
            $answers = $this->db->prepare('SELECT * FROM answers WHERE qid=:qid');
            $answers->execute(array(':qid' => $qid));
            $answersa = $answers->fetchAll(PDO::FETCH_ASSOC);

            foreach ($answersa as $row) {
                $answers_array[] = $row['answer'];
                $votes_array[] = $row['votes'];
                $pie_arr[] = "['" . $row['answer'] . "', " . $row['votes'] . "]";
            }

            $sum = array_sum($votes_array);
            foreach($votes_array as $num){
                $votes_percent[] = @round($num/$sum*100,1);
            }

            $answers_array = "'" . implode("','", $answers_array) . "'";
            $votes_percent = implode(',', $votes_percent);
            $pie_arr = implode(',', $pie_arr);

            echo $this->twig->render('chat_bar.html', array('answers_arr' => $answers_array, 'percent' => $votes_percent, 'is_pie' => $is_pie, 'pie_arr' => $pie_arr));
        }

        public function edit($qid){
            
            $question = $this->db->prepare("SELECT * FROM questions WHERE id=:qid");
            $question->execute(array(':qid' => $qid));
            if($row = $question->fetch()){
                $question_value = $row['question'];
            }

            $answers_q = "SELECT * FROM answers WHERE qid=:qid ORDER BY id";
            $answers = $this->db->prepare($answers_q);
            $answers->execute(array(':qid' => $qid));
            $answersa = $answers->fetchAll(PDO::FETCH_ASSOC);
            echo $this->twig->render('edit_poll.html', array('qid' => $qid, 'question' => $question_value, 'answers' => $answersa));
        }
        
        public function edit_exc($qid, $question, $answers_old, $answers_new = ""){

            try {
                $question_update = $this->db->prepare("UPDATE questions SET question=:question WHERE id=:qid");
                $question_update->execute(array(':qid' => $qid, ':question' => $question));

                $db_answers = $this->db->prepare("SELECT id FROM answers WHERE qid = :qid");
                $db_answers->execute(array(':qid' => $qid));
                $db_answersa = $db_answers->fetchAll(PDO::FETCH_ASSOC);
                $db_answersa_num = count($db_answersa);

                foreach ($db_answersa as $xvalue) {
                    $ansa = array(
                        ':answer' => $answers_old[$xvalue['id']],
                        ':aid' => $xvalue['id']
                    );
                    $ansa_rec[] = $answers_old[$xvalue['id']];
                    $aupdate = $this->db->prepare("UPDATE answers SET answer=:answer WHERE id=:aid");
                    $aupdate->execute($ansa);
                }

                if ($db_answersa_num < count($answers_old)) {
                    foreach ($answers_old as $xvalue) {
                        if (!in_array($xvalue, $ansa_rec)) {
                            $ansb = array(
                                ':answer' => $xvalue,
                                ':qid' => $qid
                            );
                            $new_answer_update = $this->db->prepare('INSERT INTO answers (answer,qid) VALUES (:answer,:qid)');
                            $new_answer_update->execute($ansb);
                        }
                    }
                }
                echo "question edited successfully";
            }catch (Exception $e){
                echo'Error :' . $e->getMessage();
            }

        }

        public function delete($qid){
            $bind = array(
                ':qid' => $qid
            );
            $delete_q = $this->db->prepare('DELETE FROM questions WHERE id = :qid');
            $delete_q->execute($bind);
            $delete_a = $this->db->prepare('DELETE FROM answers WHERE qid = :qid');
            $delete_a->execute($bind);
        }

        public function delete_answer($id, $question){
            $bind = array(
                ':id' => $id,
                ':qid' => $question
            );
            $del_a = $this->db->prepare('DELETE FROM answers WHERE id = :id AND qid = :qid');
            $del_a->execute($bind);
        }

    }