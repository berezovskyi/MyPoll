<?php

    class login{

        private $AuthMethod = ""; // Session or Cookie
        private $LogPage = "index.php?do=main";
        private $uname;
        private $uid;
        private $uemail;
        protected $db;

        public function __construct($db){
            $this->db = $db;
        }

        public function check($user, $pass){
            if(isset($user) && !empty($user) && isset($pass) && !empty($pass)){
                $bind = array(
                    ":user" => $user,
                    ":pass" => md5($pass)
                );
                $results = $this->db->prepare("SELECT * FROM users WHERE uname = :user AND UPASS = :pass");
                $results->bindValue(':user', $user, PDO::PARAM_STR);
                $results->bindValue(':pass', md5($pass), PDO::PARAM_STR);
                $results->execute();
                $rows = $results->fetchAll(PDO::FETCH_ASSOC);
                if(!empty($rows)){
                    //var_dump($results);
                    $this->uname = $rows[0]['uname'];
                    $this->uid = $rows[0]['ID'];
                    $this->uemail = $rows[0]['email'];
                    $this->AuthMethod = 'Session';
                }else{
                    echo general::MsgS('Wrong Username or Password', '0', 'index.php');
                    exit();
                }
            }
            $this->Authl();
        }

        public function checkLoginEachPage(){
            if(!isset($_SESSION['user']) && empty($_SESSION['user']) && !isset($_SESSION['id']) && empty($_SESSION['id'])){
                echo general::Ref('index.php');
                exit();
            }
        }

        private function Authl(){
            if($this->AuthMethod == 'Session'){
                $_SESSION['user'] = $this->uname;
                $_SESSION['id'] = $this->uid;
                $_SESSION['email'] = $this->uemail;
                echo general::Ref($this->LogPage);
            }
        }

        public function Logout($url){
            session_destroy();
            echo general::Ref($url);
        }

    }